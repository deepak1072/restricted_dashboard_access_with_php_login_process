<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/14/2017
 * Time: 12:08 AM
 */


// Initialize the session
session_start();

// Unset all of the session variables
$_SESSION = array();

// Destroy the session.
session_destroy();

// Redirect to login page
header("location: index.php");
exit;
?>
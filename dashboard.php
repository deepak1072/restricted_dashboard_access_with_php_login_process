<?php
/**
 * Created by PhpStorm.
 * User: Sandeep Maurya
 * Date: 11/13/2017
 * Time: 11:36 PM
 */
    session_start();
 require_once ("config.php");
      if(!isset($_SESSION['username'] ) || empty($_SESSION['username'])){
          header("location: index.php");
          exit;
      }


    ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>welcome to login </title>
    <!--    <link href="https://fonts.googleapis.com/css?family=Merienda" rel="stylesheet">-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
     <style type="text/css">
        .error{
            padding-bottom: 5px;
            padding-top: 5px;
            border-radius: 0px;
        }
        span#logo{

            margin: 0;
            text-shadow: 2px 2px 3px rgba(111, 108, 108, 0.6);
            font-size: 42px;
            margin-left: -8px;
            font-weight: 700;

        }
        .navbar-brand {
            color: #26617d;
            margin-left: 23%;
            margin-bottom: 2%;

        }

        .navbar-brand:hover{
            color: #4c99ab;
        }

        .navbar-brand img{
            display: inline-block;
        }
        hr{
            border-color: #4e9aac;
        }
        a:hover{
            text-decoration: none;
        }
    </style>
</head>
<body>
</body>
   <div class="container">
       <div class="row">
           <div class="col-xs-12">
               <ul style="float: right;list-style: none;">
                   <li>
                       <a class="btn btn-default" href="logout.php">logout</a>
                   </li>
               </ul>
           </div>
       </div>
   </div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center" style="margin: 50px auto;">
            <h1>Welcome to Dashboard </h1>
            <h2>This page is restricted from all</h2>
        </div>
    </div>
</div>
</html>
